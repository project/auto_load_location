CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Auto Load Location allows you to load address and geolocation fields from one to another content types. When a field is configured to be original loading source and a content type is checked as destination, it will allow users with proper permissions to use the "Load address" and "Load geolocation" buttons. These buttons are added along with configured field when adding/editing nodes. When editing a node (from any content types that have been enabled), first address and geolocation fields will be auto loaded with data from entered field.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
------------

After enable the module, you can config field and content types at /admin/config/auto_load_location.


MAINTAINERS
-----------

Current maintainers:
 * Hoa Dang Nguyen (hoaqtcom) - https://www.drupal.org/u/hoaqtcom
