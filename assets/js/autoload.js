(function($, Drupal, drupalSettings) {

  // Ajax request when user click load adress button
  $('#load-address-button').click(function() {
    var nid = get_nid();
    get_location_data(nid, 'address');
  });

  // Ajax request when user click load geolocation button
  $('#load-geolocation-button').click(function() {
    var nid = get_nid();
    get_location_data(nid, 'geolocation');
  });

  // Get nid from reference field 
  function get_nid() {
    var location_field_name = drupalSettings.location_field_name;
    var field_name = location_field_name.replace('_', '-');
    var field_id = jQuery('#edit-'+field_name+'-0-target-id').val();
    if (field_id) {
      var node_name_id = field_id.match(/^(.*?)\(([0-9]+)\)$/);
      var nid = node_name_id[2];
      return nid;
    }
    else {
      return false;
    }
  }

  // Trigger onchange event when user click load button
  function change_event(e, response, type) {

    let event = new Event("change", {
        bubbles: true,
        cancelable: true
    });

    let element = document.querySelector(e);
    element.dispatchEvent(event);

    if (type == 'address') {
      var check_country_loading_status = setInterval(function() {

        // Fill all address fields with available data from entity referenece field
        if ($('js-form-item-field-location-0-address-country-code .ajax-progress').length === 0) {
          clearInterval(check_country_loading_status);

          $('.js-form-item-field-location-0-address-administrative-area select').val(response.location_administrative_area);
          $('.js-form-item-field-location-0-address-locality input').val(response.location_locality);
          $('.js-form-item-field-location-0-address-dependent-locality input').val(response.location_dependent_locality);
          $('.js-form-item-field-location-0-address-postal-code input').val(response.location_postal_code);
          $('.js-form-item-field-location-0-address-sorting-code input').val(response.location_sorting_code);
          $('.js-form-item-field-location-0-address-address-line1 input').val(response.location_address_line1);
          $('.js-form-item-field-location-0-address-address-line2 input').val(response.location_address_line2);
          $('.js-form-item-field-location-0-address-organization input').val(response.location_organization);
          $('.js-form-item-field-location-0-address-additional-name input').val(response.location_additional_name);
          $('.js-form-item-field-location-0-address-given-name input').val(response.location_given_name);
          $('.js-form-item-field-location-0-address-family-name input').val(response.location_family_name);
        }
      }, 2000);
    }
  }

  // Handle the Ajax call and get the response
  function get_location_data(nid, type) {

    if (!$('#load-'+type+' #loading-image').length) {
      $('#load-'+type).append('<span id="loading-image"> Loading...</span>');
    }

    $('#load-'+type+' #loading-image').show();
    var $th = $('#load-'+type);

    if (!$th.hasClass('pending') ) {
      
      $th.addClass('pending');
      
      $.ajax({
        url: Drupal.url('node/'+ nid +'/'+type),
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function(response) {

          $th.removeClass('pending');
          setTimeout(function() {

            if (type == 'address') {
              $('.js-form-item-field-location-0-address-country-code select').val(response.location_country_code);
              change_event('.js-form-item-field-location-0-address-country-code select', response, type);
            }
            else if (type == 'geolocation') {
              $('.js-form-item-field-location-geo-0-lat input').val(response.location_lat);
              $('.js-form-item-field-location-geo-0-lng input').val(response.location_lon);
            }
          }, 500);
        },
        complete: function() {
          $('#load-'+type+' #loading-image').text(' Done');
          setTimeout(function() {
            $('#load-'+type+' #loading-image').hide();
            $('#load-'+type+' #loading-image').text(' Loading...');
          }, 1000);
        },
        error: function(xhr, status, error) {

          //var err = eval("(" + xhr.responseText + ")");
          //console.log(err.Message);
        }
      });
    }
  }
})(jQuery, Drupal, window.drupalSettings);
