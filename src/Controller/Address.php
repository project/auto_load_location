<?php

namespace Drupal\auto_load_location\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\auto_load_location\Controller\Permission;

class Address extends ControllerBase {

    /**
    * Get Address from origin content type.
    */
    public function getAddress($variable) {

        // Get node object.
        $node = Node::load($variable);

        if (!$node) {
            return new JsonResponse([]);
        }
        
        // Get first address field name
        foreach ($node->getFields() as $name => $field) {
            $field_name = $field->getName();
            if (preg_match('/field_/', $field_name, $matches)) {

                if ($field->getFieldDefinition()->getType() == "address") {
                    $address_field_name = $field->getName();
                    break;
                }
            }
        }

        // Check permission to display address data.
        $permission = new Permission();
        $has_permission = $permission->getPermission();

        if (!$has_permission) {
            // Show access denided page for anonymous users.
            throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
        }
        else {
            $result = ['address_field_name' => $address_field_name];

            // Get address info from node.
            if ($node->hasField($address_field_name) && !$node->get($address_field_name)->isEmpty()) {

                $full_country_list = \Drupal::service('country_manager')->getList();
                $location_country_code = $node->get($address_field_name)->first()->get("country_code")->getValue();

                $result['location_country_code'] = $location_country_code;
                $result['location_country'] = t($full_country_list[$location_country_code]->getUntranslatedString(), [], ['langcode'=>'en']);
                $result['location_administrative_area'] = $node->get($address_field_name)->first()->get("administrative_area")->getValue();
                $result['location_locality'] = $node->get($address_field_name)->first()->get("locality")->getValue();
                $result['location_dependent_locality'] = $node->get($address_field_name)->first()->get("dependent_locality")->getValue();
                $result['location_postal_code'] = $node->get($address_field_name)->first()->get("postal_code")->getValue();
                $result['location_sorting_code'] = $node->get($address_field_name)->first()->get("sorting_code")->getValue();
                $result['location_address_line1'] = $node->get($address_field_name)->first()->get("address_line1")->getValue();
                $result['location_address_line2'] = $node->get($address_field_name)->first()->get("address_line2")->getValue();
                $result['location_organization'] = $node->get($address_field_name)->first()->get("organization")->getValue();
                $result['location_given_name'] = $node->get($address_field_name)->first()->get("given_name")->getValue();
                $result['location_additional_name'] = $node->get($address_field_name)->first()->get("additional_name")->getValue();
                $result['location_family_name'] = $node->get($address_field_name)->first()->get("family_name")->getValue();
            }

            return new JsonResponse($result);
        }

    }

    /**
    * Get Latitude and Longitude from Geolocation field of origin content type.
    */
    public function getLatLon($variable) {

        // Get node object.
        $node = Node::load($variable);

        if (!$node) {
            return new JsonResponse([]);
        }

        // Get first geolocation field name
        foreach ($node->getFields() as $name => $field) {
            $field_name = $field->getName();
            if (preg_match('/field_/', $field_name, $matches)) {

                if ($field->getFieldDefinition()->getType() == "geolocation") {
                    $latlon_field_name = $field->getName();
                    break;
                }
            }
        }

        $permission = new Permission();
        $has_permission = $permission->getPermission($variable);

        if (!$has_permission) {
            throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
        }
        else {
            $result = ['latlon_field_name' => $latlon_field_name];

            // Get geolocation info from node.
            if ($node->hasField($latlon_field_name) && !$node->get($latlon_field_name)->isEmpty()) {

                $result['location_lat'] = $node->get($latlon_field_name)->first()->getValue()["lat"];
                $result['location_lon'] = $node->get($latlon_field_name)->first()->getValue()["lng"];
            }
            return new JsonResponse($result);
        }

    }

}


