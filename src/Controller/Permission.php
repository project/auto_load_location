<?php

namespace Drupal\auto_load_location\Controller;

use Drupal\Core\Controller\ControllerBase;

class Permission extends ControllerBase {
    /**
     * Check user role and only authenticated user to get the ajax result.
     */
    public function getPermission() {
        $roles = \Drupal::currentUser()->getRoles();

        if (in_array('authenticated', $roles)) {
            return true;
        }
        return false;
    }

}


