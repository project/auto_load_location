<?php

namespace Drupal\auto_load_location\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'auto_load_location.settings',
    ];
  }

  /**
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'auto_load_location_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('auto_load_location.settings');
    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
  
    foreach($types as $type) {
      $content_types[$type->id()] = $type->label();
    }

    // Entity reference field.
    $form['entity_reference_field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity Reference Field Name (Referenced to original content type)'),
      '#description' => $this->t('Enter the field name inside enabled content types below that referenced to the original content type. This is field to get the Node ID of referenced node. For example: <em>field_node_reference</em>'),
      '#default_value' => $config->get('entity_reference_field_name'),
    ];

    // List all content types. 
    $form['enabled_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => t('Enabled Content Types (Destination content type)'),
      '#description' => t('Content types that are enabled to have Auto Load Location added.'),
      '#options' => $content_types,
      '#default_value' => $config->get('enabled_content_types') ? : [],
    ];

    // Enable auto loading on editing node. 
    $form['enabled_autoload_edit'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled auto load location on node edit form'),
      '#description' => t('Check this to load location on node edit form automatically. By default, buttons will be used manually.'),
      '#default_value' => $config->get('enabled_autoload_edit'),
    ];

    // Form submit handlers.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate the form.
   * 
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * 
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $reference_field = $form_state->getValue('entity_reference_field_name');

    if ( !empty($reference_field) && (strlen($reference_field) < 7 || !preg_match('/field_/', $reference_field, $matches)) ) {
      // Set an error for the form element with a key of "title".
      $form_state->setErrorByName('title', $this->t('The field name must be at least 7 characters long and start with with field_'));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('auto_load_location.settings')
      ->set('entity_reference_field_name', $form_state->getValue('entity_reference_field_name'))
      ->set('enabled_content_types', $form_state->getValue('enabled_content_types'))
      ->set('enabled_autoload_edit', $form_state->getValue('enabled_autoload_edit'))
      ->save();

    // Display the results.
    $messenger = \Drupal::messenger();

    if (!empty($form_state->getValue('entity_reference_field_name'))) {
      $messenger->addStatus(['#markup' => t('Entity reference field: ').'<em>'.$form_state->getValue('entity_reference_field_name'). '</em>']);
    }

    $content_types = $form_state->getValue('enabled_content_types');
    if (!empty(array_filter($content_types))) {
      $messenger->addMessage(t('Enabled content types: '));
      foreach ($content_types as $value) {
        if (!empty($value)) {
          $messenger->addStatus(['#markup' => '<em>' . $value . '</em>']);
        }
      }
    }

  }

}
